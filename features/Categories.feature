Feature: Category Scenarios
  Scenario: 1 verify whether the added product and quantity are displayed in the shopping Basket
    Given I go to page "https://uk.rs-online.com/web/generalDisplay.html?id=new"
    When I go to "Batteries" category
    And I add products to the basket
      | Product Description   | Quantity |
      |  RS Pro QX 15000mAh 5V Power Bank Portable Charger | 2   |
    Then Item should be displayed in the shopping Basket
      | Product Description   | Quantity |
      |  RS Pro QX 15000mAh 5V Power Bank Portable Charger | 2   |

  Scenario: 2 Verify whether the products are sorted from Low to High
    Given I go to page "https://uk.rs-online.com/web/generalDisplay.html?id=new"
    When I go to "Batteries" category
    And I Sort the Products from "Price: Low to High"
    Then Item should be sorted from "Price: Low to High"


  Scenario Outline: 3 Verify whether the products are displayed based on selected Results Per Page
    Given I go to page "https://uk.rs-online.com/web/generalDisplay.html?id=new"
    When I go to "Batteries" category
    And I Change the Results per page "<ResultsPerPage>"
    Then Products should be displayed according to "<ResultsPerPage>"

    Examples:
    |ResultsPerPage|
    |50            |
    |100           |
    |20            |

  Scenario: 4 Verify whether the Compare button is clickable and navigating to the compare page
    Given I go to page "https://uk.rs-online.com/web/generalDisplay.html?id=new"
    When I go to "Batteries" category
    And I select Products to compare
      |  RS Pro VPP-005 8400mAh 5V Power Bank Portable Charger |
      |  RS Pro QX 15000mAh 5V Power Bank Portable Charger |
    Then compare button should be clickable and navigates to compare page
      |  RS Pro VPP-005 8400mAh 5V Power Bank Portable Charger |
      |  RS Pro QX 15000mAh 5V Power Bank Portable Charger |

  Scenario: 5 Verify whether the products are displayed based on the Brand selected
    Given I go to page "https://uk.rs-online.com/web/generalDisplay.html?id=new"
    When I go to "Batteries" category
    And I select the Brand "RS Pro"
    Then the Brand Products should be displayed "RS Pro"