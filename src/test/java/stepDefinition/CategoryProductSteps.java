package stepDefinition;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.interactions.internal.Locatable;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CategoryProductSteps {

    WebDriver driver;


    @Given("^I go to page \"(.*?)\"$")
    public void i_go_to_page(String Url) throws Throwable {
        System.setProperty("webdriver.chrome.driver","C:\\JavaProject\\comnewautomationcucumber\\chromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(Url);
    }

    @When("^I go to \"(.*?)\" category$")
    public void i_go_to_category(String category) throws Throwable {
        String Page_title;
        driver.findElement(By.linkText(category)).click();
        Page_title = driver.getTitle();
        Assert.assertTrue("Not Found", Page_title.contains(category));
    }

    @When("^I add products to the basket$")
    public void i_add_products_to_the_basket(DataTable table) throws Throwable {
        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        String linkText = data.get(0).get("Product Description");
        String linkQty = data.get(0).get("Quantity");
        List<WebElement> TableRows = driver.findElements(By.xpath("//tr[@class='resultRow']"));
        List<WebElement> TextBoxes = driver.findElements(By.xpath("//input[@class ='cartInput']"));
        List<WebElement> AddButtons = driver.findElements(By.xpath("//input[@value ='Add']"));
        for(int i=0; i<TableRows.size();i++) {
            if(TableRows.get(i).getText().contains(linkText))
            {
                TextBoxes.get(i).clear();
                TextBoxes.get(i).sendKeys(linkQty);
                AddButtons.get(i).click();
                break;
            }
        }
    }
    @Then("^Item should be displayed in the shopping Basket$")
    public void item_should_be_displayed_in_the_shopping_Basket(DataTable BasketTable) throws Throwable {
        List<Map<String, String>> basketData = BasketTable.asMaps(String.class, String.class);
        String BasketLinkText = basketData.get(0).get("Product Description");
        String BasketLinkQty = basketData.get(0).get("Quantity");
        driver.findElement(By.xpath("//i[@class ='icon-cart']")).click();
        List<WebElement> TableRows = driver.findElements(By.xpath("//tr[@class='dataRow  lineRow']"));
        List<WebElement> TextBoxes = driver.findElements(By.xpath("//input[@class ='inputText costColumn floatLeft']"));
        for(int i=0; i<TableRows.size();i++) {
            Assert.assertTrue("Not Found",TableRows.get(i).getText().contains(BasketLinkText));
            Assert.assertTrue("Not Found",TextBoxes.get(i).getAttribute("value").contains(BasketLinkQty));
        }
    }

    @When("^I Sort the Products from \"(.*?)\"$")
    public void i_Sort_the_Products_from(String SortOption) throws Throwable {
        WebElement dropdown =  driver.findElement(By.xpath("//span[contains(text(),'Sort by')]/parent::li/following-sibling::li[@class='defaultItem']"));
        Mouse mouse = ((HasInputDevices) driver).getMouse();
        Locatable hoverItem = (Locatable) dropdown;
        mouse.mouseMove(hoverItem.getCoordinates());
        WebElement beginner = driver.findElement(By.xpath("//span[text()='Price: Low to High']"));
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(beginner));
        Locatable clickItem = (Locatable) beginner;
        mouse.mouseDown(clickItem.getCoordinates());
        mouse.mouseUp(clickItem.getCoordinates());
    }

    @Then("^Item should be sorted from \"(.*?)\"$")
    public void item_should_be_sorted_from(String SortOption) throws Throwable {
        List<WebElement> SortedPrices = driver.findElements(By.xpath("//span[@class='price right5']"));
        List<Double> PriceValues = new ArrayList<Double>();
        for (WebElement sp: SortedPrices
             ) {
            String str = sp.getText();
            str = str.replace("£", "");
            double pValue = Double.parseDouble(str);
            PriceValues.add(pValue);
        }
        double basePrice=PriceValues.get(0);
        double nextPrice;
        boolean status = true;
        for (Double pv : PriceValues
             ) {
            nextPrice = pv;
            if(nextPrice >= basePrice){
                basePrice = nextPrice;
                continue;
            }
            else {
                status = false;
                break;
            }
        }
        Assert.assertTrue(status);
    }

    @When("^I Change the Results per page \"(.*?)\"$")
    public void i_Change_the_Results_per_page(int resultsPerPage) throws Throwable {
        WebElement dropdown =  driver.findElement(By.xpath("//span[contains(text(),'Results per page')]/parent::li/following-sibling::li[@class='defaultItem']"));
        Mouse mouse = ((HasInputDevices) driver).getMouse();
        Locatable hoverItem = (Locatable) dropdown;
        mouse.mouseMove(hoverItem.getCoordinates());
        WebElement beginner = driver.findElement(By.xpath("//span[text()='"+ resultsPerPage +"']"));
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(beginner));
        Locatable clickItem = (Locatable) beginner;
        mouse.mouseDown(clickItem.getCoordinates());
        mouse.mouseUp(clickItem.getCoordinates());
    }

    @Then("^Products should be displayed according to \"(.*?)\"$")
    public void products_should_be_displayed_according_to(int resultsPerPage) throws Throwable {
        List<WebElement> TableRows = driver.findElements(By.xpath("//tr[@class='resultRow']"));
        int noOfRowsDisplayed = TableRows.size();
        boolean status = true;
        if(noOfRowsDisplayed > resultsPerPage)
        {
            status = false;
        }
        Assert.assertTrue(status);
    }

    @When("^I select Products to compare$")
    public void i_select_Products_to_compare(List<String> ProductsToCompare) throws Throwable {

        List<WebElement> TableRows = driver.findElements(By.xpath("//tr[@class='resultRow']"));
        List<WebElement> CheckBoxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (String product: ProductsToCompare
             ) {
            for (int i=0; i<TableRows.size(); i++){
                if(TableRows.get(i).getText().contains(product))
                {
                    CheckBoxes.get(i).click();
                    break;
                }
            }
        }
    }

    @Then("^compare button should be clickable and navigates to compare page$")
    public void compare_button_should_be_clickable_and_navigates_to_compare_page(List<String> SelectedProductsToCompare) throws Throwable {
        driver.findElement(By.id("compareSelectedBtnEnabled")).click();
        List<WebElement> ComparedTableRows = driver.findElements(By.xpath("//table[@class ='compareSubTable']/tbody/tr/td/div/a"));
        int count = 0;
        for (String selectedProduct: SelectedProductsToCompare
             ) {
                for(int i=0; i<ComparedTableRows.size();i++)
                {
                    if (ComparedTableRows.get(i).getText().contains(selectedProduct))
                    {
                        count = count + 1;
                    }
                }
        }
        Assert.assertTrue(count == SelectedProductsToCompare.size());
    }

    @When("^I select the Brand \"(.*?)\"$")
    public void i_select_the_Brand(String BrandToSelect) throws Throwable {
        driver.findElement(By.xpath("//div[@class='collapseIcon']")).click();
        driver.findElement(By.xpath("//div[@class='brandLink']/a[@title='RS Pro']")).click();
    }

    @Then("^the Brand Products should be displayed \"(.*?)\"$")
    public void the_Brand_Products_should_be_displayed(String SelectedBrand) throws Throwable {
        List<WebElement> TableRows = driver.findElements(By.xpath("//tr[@class='resultRow']"));
        boolean status = true;
        for (WebElement  Product: TableRows
             ) {
            if(!(Product.getText().contains(SelectedBrand)))
            {
                status = false;
            }
        }
        Assert.assertTrue(status);
    }
    
}
